import model.Email;
import model.User;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import util.PropertyLoader;
import util.TestUtils;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aviskase on 21/07/16.
 */
public class GmailTest {
    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeMethod
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        // Disable notifications
        prefs.put("profile.default_content_setting_values.notifications", 2);
        options.setExperimentalOption("prefs", prefs);

        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 20);
    }

    @Test
    public void testEmailIsReceived_SubjectIsCorrect() {
        User sender = new User(
                PropertyLoader.loadAppProperty("user1.login"),
                PropertyLoader.loadAppProperty("user1.password"));
        User recipient = new User(
                PropertyLoader.loadAppProperty("user2.login"),
                PropertyLoader.loadAppProperty("user2.password"));

        String subject = "<script>alert(\"I know JavaScript\")</script>";
        Email email = new Email(sender, recipient.getEmail(), subject, "Test email");
        try {
            TestUtils.sendEmail(email);
        } catch (MessagingException e) {
            Assert.fail("Cannot send mail, test is aborted", e);
        }

        loginAs(recipient);

        String emailLinkLocator = "//*[@role='main']//table//*[@email='"
                + sender.getEmail()
                + "']/following::*[@role='link']";
        driver.findElement(By.xpath(emailLinkLocator)).click();
        WebElement emailSubject = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@role='main']//h2"))
        );
        String receivedEmailSubject = emailSubject.getText();
        deleteCurrentEmail();
        Assert.assertEquals(email.getSubject(), receivedEmailSubject);
    }

    private void loginAs(User user) {
        driver.get(PropertyLoader.loadAppProperty("site.url"));

        WebElement emailField = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("Email"))
        );
        emailField.clear();
        emailField.sendKeys(user.getEmail());
        emailField.sendKeys(Keys.RETURN);

        WebElement passwordField = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("Passwd"))
        );
        passwordField.clear();
        passwordField.sendKeys(user.getPassword());
        passwordField.sendKeys(Keys.RETURN);

        // Wait for logo
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='#inbox']"))
        );

    }

    private void logout() {
        WebElement userIcon = driver.findElement(By.xpath("//a[contains(@href,'SignOutOptions')]"));
        userIcon.click();
        WebElement logoutButton = wait.until(
                ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@href,'Logout')]"))
        );
        logoutButton.click();
        driver.manage().deleteAllCookies();
    }

    private void deleteCurrentEmail() {
        String deleteButtonLocator = "//*[@gh='mtb']//*[@act='10']";
        driver.findElement(By.xpath(deleteButtonLocator)).click();
        wait.until(
                ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@role='main']//h2"))
        );
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
