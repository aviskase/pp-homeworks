package util;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by aviskase on 24/07/16.
 */
public class PropertyLoader {
    private static final String APP_PROPS_FILE = "/application.properties";
    private static final String MAIL_PROPS_FILE = "/mail.properties";

    public static String loadAppProperty(String name) {
        Properties props = loadAppProperties();
        String value = null;
        if (name != null) {
            value = props.getProperty(name);
        }
        return value;
    }

    public static Properties loadAppProperties() {
        return loadProperties(APP_PROPS_FILE);
    }


    public static Properties loadMailProperties() {
        return loadProperties(MAIL_PROPS_FILE);
    }

    private static Properties loadProperties(String file) {
        Properties props = new Properties();

        try {
            props.load(PropertyLoader.class.getResourceAsStream(file));
        } catch (IOException e) {
            throw new IllegalStateException("Cannot load properties from file: " + file, e);
        } catch (NullPointerException e) {
            throw new IllegalStateException("Properties file \"" + file + "\" is not found", e);
        }

        return props;
    }
}