package util;

import model.Email;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by aviskase on 24/07/16.
 */
public class TestUtils {
    public static void sendEmail(final Email email) throws MessagingException {
        Properties mailProperties = PropertyLoader.loadMailProperties();

        Session mailSession = Session.getDefaultInstance(mailProperties, new Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email.getSender().getEmail(), email.getSender().getPassword());
            }
        });

        Message message = new MimeMessage(mailSession);
        try {
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getRecipient()));
            message.setSubject(email.getSubject());
            message.setText(email.getBody());
            Transport.send(message);
        } catch (MessagingException e) {
            throw new MessagingException("Cannot send email", e);
        }
    }
}
