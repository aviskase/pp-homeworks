package model;

/**
 * Created by aviskase on 21/07/16.
 */
public class Email {
    private String subject;
    private String body;
    private String recipient;
    private User sender;

    public Email(User sender, String recipient, String subject, String body) {
        this.sender = sender;
        this.recipient = recipient;
        this.subject = subject;
        this.body = body;
    }

    public String getSubject() { return subject; }

    public String getBody() {
        return body;
    }

    public String getRecipient() {
        return recipient;
    }

    public User getSender() { return  sender; }
}
