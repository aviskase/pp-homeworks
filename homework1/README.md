Проверить, что письмо с темой `<script>alert(“I know JavaScript”)</script>` приходит на почту.

* нужно зайти на gmail.com
* залогиниться
* отправить письмо себе на ящик (любым способом, предпочтительно отправить письмо с другого аккаунта/другой почты, с темой в письме `<script>alert(“I know JavaScript”)</script>)`
* проверить, что письмо пришло во входящие
* проверить, что тема письма совпадает с той, которую ты отправила